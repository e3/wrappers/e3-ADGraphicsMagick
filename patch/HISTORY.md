# rename.p0.patch

The GraphicksMagick components of ADSupport have a lot of source files, and
since require builds a single shared library out of the sources, we end up
with some collisions (i.e. if we compile two separate `gradient.c`, then the
second one will overwrite the first's compiled `gradient.o`). The solution we
chose was to renamed some of the files in order to avoid this.

* created by Simon Rose (simon.rose@ess.eu)
* October 22, 2020
