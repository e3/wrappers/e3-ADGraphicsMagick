#
#  Copyright (c) 2019      Jeong Han Lee
#  Copyright (c) 2019      European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Friday, September 13 15:16:36 CEST 2019
# version : 0.0.8
#

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky


# We need ADSupport for its jpeg, xml2, etc. libraries. For consistency, we
# will keep the exact same version number.
REQUIRED += adsupport
ifneq ($(strip $(ADSUPPORT_DEP_VERSION)),)
adsupport_VERSION = $(ADSUPPORT_DEP_VERSION)
endif


APP:=supportApp

OS_default:=os/default
OS_Linux:=os/Linux

USR_CFLAGS   += -Wno-unused-variable
USR_CFLAGS   += -Wno-unused-function
USR_CFLAGS   += -Wno-unused-but-set-variable
USR_CPPFLAGS += -Wno-unused-variable
USR_CPPFLAGS += -Wno-unused-function
USR_CPPFLAGS += -Wno-unused-but-set-variable

USR_INCLUDES += -I$(PREFIX)/include
# Ubuntu needs the following option
USR_LDFLAGS += -Wl,--no-as-needed


GMTOP:=$(APP)/GraphicsMagickSrc


GM_INCS += magick_config.h
GM_INCS += magick_config_Win32.h
GM_INCS += magick_config_Darwin.h
GM_INCS += magick_config_Linux.h
GM_INCS += api.h
GM_INCS += enum_strings.h
GM_INCS += common.h
GM_INCS += magick_types.h
GM_INCS += analyze.h
GM_INCS += image.h
GM_INCS += forward.h
GM_INCS += colorspace.h
GM_INCS += error.h
GM_INCS += log.h
GM_INCS += timer.h
GM_INCS += attribute.h
GM_INCS += average.h
GM_INCS += blob.h
GM_INCS += cdl.h
GM_INCS += channel.h
GM_INCS += color.h
GM_INCS += color_lookup.h
GM_INCS += colormap.h
GM_INCS += command.h
GM_INCS += compare.h
GM_INCS += composite.h
GM_INCS += compress.h
GM_INCS += confirm_access.h
GM_INCS += constitute.h
GM_INCS += decorate.h
GM_INCS += delegate.h
GM_INCS += deprecate.h
GM_INCS += describe.h
GM_INCS += draw.h
GM_INCS += effect.h
GM_INCS += enhance.h
GM_INCS += error.h
GM_INCS += fx.h
GM_INCS += gem.h
GM_INCS += gradient.h
GM_INCS += hclut.h
GM_INCS += image.h
GM_INCS += list.h
GM_INCS += log.h
GM_INCS += magic.h
GM_INCS += magick.h
GM_INCS += memory.h
GM_INCS += module.h
GM_INCS += monitor.h
GM_INCS += montage.h
GM_INCS += operator.h
GM_INCS += paint.h
GM_INCS += pixel_cache.h
GM_INCS += pixel_iterator.h
GM_INCS += plasma.h
GM_INCS += profile.h
GM_INCS += random.h
GM_INCS += quantize.h
GM_INCS += registry.h
GM_INCS += render.h
GM_INCS += resize.h
GM_INCS += resource.h
GM_INCS += shear.h
GM_INCS += signature.h
GM_INCS += statistics.h
GM_INCS += studio.h
GM_INCS += symbols.h
GM_INCS += texture.h
GM_INCS += timer.h
GM_INCS += transform.h
GM_INCS += type.h
GM_INCS += utility.h
GM_INCS += version.h

GM_SRCS += analyze_Linux.c
GM_SRCS += animate.c
GM_SRCS += annotate.c
GM_SRCS += attribute.c
GM_SRCS += average.c
GM_SRCS += bit_stream.c
GM_SRCS += blob.c
GM_SRCS += cdl.c
GM_SRCS += channel.c
GM_SRCS += color.c
GM_SRCS += colormap.c
GM_SRCS += colorspace.c
GM_SRCS += color_lookup.c
GM_SRCS += command.c
GM_SRCS += compare.c
GM_SRCS += composite.c
GM_SRCS += compress.c
GM_SRCS += confirm_access.c
GM_SRCS += constitute.c
GM_SRCS += decorate.c
GM_SRCS += delegate.c
GM_SRCS += deprecate.c
GM_SRCS += describe.c
GM_SRCS += display.c
GM_SRCS += draw.c
GM_SRCS += effect.c
GM_SRCS += enhance.c
GM_SRCS += enum_strings.c
GM_SRCS += error.c
GM_SRCS += export.c
GM_SRCS += floats.c
GM_SRCS += fx.c
GM_SRCS += gem.c
GM_SRCS += gradient.c
GM_SRCS += hclut.c
GM_SRCS += image.c
GM_SRCS += import.c
GM_SRCS += list.c
GM_SRCS += locale.c
GM_SRCS += log.c
GM_SRCS += magic.c
GM_SRCS += magick.c
GM_SRCS += magick_endian.c
GM_SRCS += map.c
GM_SRCS += memory.c
GM_SRCS += module.c
GM_SRCS += monitor.c
GM_SRCS += montage.c
GM_SRCS += nt_base.c
GM_SRCS += nt_feature.c
GM_SRCS += omp_data_view.c
GM_SRCS += operator.c
GM_SRCS += paint.c
GM_SRCS += pixel_cache.c
GM_SRCS += pixel_iterator.c
GM_SRCS += plasma.c
GM_SRCS += PreRvIcccm.c
GM_SRCS += profile.c
GM_SRCS += quantize.c
GM_SRCS += random.c
GM_SRCS += registry.c
GM_SRCS += render.c
GM_SRCS += resize.c
GM_SRCS += resource.c
GM_SRCS += segment.c
GM_SRCS += semaphore.c
GM_SRCS += shear.c
GM_SRCS += signature.c
GM_SRCS += statistics.c
GM_SRCS += tempfile.c
GM_SRCS += texture.c
GM_SRCS += timer.c
GM_SRCS += transform.c
GM_SRCS += tsd.c
GM_SRCS += type.c
GM_SRCS += unix_port.c
GM_SRCS += utility.c
GM_SRCS += version.c
GM_SRCS += widget.c
GM_SRCS += xwindow.c

USR_INCLUDES += -I$(where_am_I)$(GMTOP)
USR_INCLUDES += -I$(where_am_I)$(GMTOP)/lcms/include
USR_INCLUDES += -I$(where_am_I)$(GMTOP)/ttf/include
USR_INCLUDES += -I$(where_am_I)$(GMTOP)/bzlib

USR_CFLAGS += -D_MAGICKLIB_
USR_CFLAGS += -DHAVE_VSNPRINTF
ifeq ($(GRAPHICSMAGICK_PREFIX_SYMBOLS),YES)
	USR_CFLAGS += -DPREFIX_MAGICK_SYMBOLS
endif

HEADERS += $(addprefix $(GMTOP)/magick/, $(GM_INCS))

SOURCES += $(addprefix $(GMTOP)/magick/, $(GM_SRCS))

LIB_SYS_LIBS += Xext bz2 freetype lcms2

############ Magick++

USR_INCLUDES += -I$(where_am_I)$(GMTOP)/Magick++/lib

GMXX_INCS += Magick++.h
GMXX_INCS += Magick++/Include.h
GMXX_INCS += Magick++/Image.h
GMXX_INCS += Magick++/Pixels.h
GMXX_INCS += Magick++/STL.h
GMXX_INCS += Magick++/Blob.h
GMXX_INCS += Magick++/Color.h
GMXX_INCS += Magick++/Drawable.h
GMXX_INCS += Magick++/BlobRef.h
GMXX_INCS += Magick++/CoderInfo.h
GMXX_INCS += Magick++/Exception.h
GMXX_INCS += Magick++/Geometry.h
GMXX_INCS += Magick++/ImageRef.h
GMXX_INCS += Magick++/Functions.h
GMXX_INCS += Magick++/Montage.h
GMXX_INCS += Magick++/Options.h
GMXX_INCS += Magick++/Thread.h
GMXX_INCS += Magick++/TypeMetric.h

GMXX_SRCS += Blob.cpp
GMXX_SRCS += BlobRef.cpp
GMXX_SRCS += CoderInfo.cpp
GMXX_SRCS += Color.cpp
GMXX_SRCS += Drawable.cpp
GMXX_SRCS += Exception.cpp
GMXX_SRCS += Functions.cpp
GMXX_SRCS += Geometry.cpp
GMXX_SRCS += Image.cpp
GMXX_SRCS += ImageRef.cpp
GMXX_SRCS += Montage.cpp
GMXX_SRCS += Options.cpp
GMXX_SRCS += Pixels.cpp
GMXX_SRCS += STL.cpp
GMXX_SRCS += Thread.cpp
GMXX_SRCS += TypeMetric.cpp

KEEP_HEADER_SUBDIRS += $(GMTOP)


HEADERS += $(addprefix $(GMTOP)/Magick++/lib/, $(GMXX_INCS))

SOURCES += $(addprefix $(GMTOP)/Magick++/lib/, $(GMXX_SRCS))

############## Coders stuff

USR_INCLUDES += -I$(where_am_I)$(GMTOP)/jbig/libjbig
USR_INCLUDES += -I$(where_am_I)$(GMTOP)/jp2/src/libjasper/include
USR_INCLUDES += -I$(where_am_I)$(GMTOP)/png
#USR_INCLUDES += -I$(where_am_I)$(GMTOP)/webp/src
USR_INCLUDES += -I$(where_am_I)$(GMTOP)/wmf/include
USR_INCLUDES += -I$(where_am_I)$(APP)/xml2Src/os/default


CODER_INCS += static.h

CODER_SRCS += art.c
CODER_SRCS += avi.c
CODER_SRCS += avs.c
CODER_SRCS += bmp.c
CODER_SRCS += cals.c
CODER_SRCS += caption.c
CODER_SRCS += cineon.c
CODER_SRCS += clipboard.c
CODER_SRCS += cmyk.c
CODER_SRCS += cut.c
CODER_SRCS += dcm.c
CODER_SRCS += dcraw.c
CODER_SRCS += dib.c
CODER_SRCS += dps.c
CODER_SRCS += dpx.c
CODER_SRCS += emf.c
CODER_SRCS += ept.c
CODER_SRCS += fax.c
CODER_SRCS += fits.c
CODER_SRCS += fpx.c
CODER_SRCS += gif.c
CODER_SRCS += coders_gradient.c
CODER_SRCS += gray.c
CODER_SRCS += hdf.c
CODER_SRCS += histogram.c
CODER_SRCS += hrz.c
CODER_SRCS += html.c
CODER_SRCS += icon.c
CODER_SRCS += identity.c
CODER_SRCS += info.c
CODER_SRCS += coders_jbig.c
CODER_SRCS += jnx.c
CODER_SRCS += jp2.c
CODER_SRCS += jpeg.c
CODER_SRCS += label.c
CODER_SRCS += coders_locale.c
CODER_SRCS += logo.c
CODER_SRCS += mac.c
CODER_SRCS += coders_map.c
CODER_SRCS += mat.c
CODER_SRCS += matte.c
CODER_SRCS += meta.c
CODER_SRCS += miff.c
CODER_SRCS += mono.c
CODER_SRCS += mpc.c
CODER_SRCS += mpeg.c
CODER_SRCS += mpr.c
CODER_SRCS += msl.c
CODER_SRCS += mtv.c
CODER_SRCS += mvg.c
CODER_SRCS += null.c
CODER_SRCS += otb.c
CODER_SRCS += palm.c
CODER_SRCS += pcd.c
CODER_SRCS += pcl.c
CODER_SRCS += pcx.c
CODER_SRCS += pdb.c
CODER_SRCS += pdf.c
CODER_SRCS += pict.c
CODER_SRCS += pix.c
CODER_SRCS += coders_plasma.c
CODER_SRCS += png.c
CODER_SRCS += pnm.c
CODER_SRCS += preview.c
CODER_SRCS += ps.c
CODER_SRCS += ps2.c
CODER_SRCS += ps3.c
CODER_SRCS += psd.c
CODER_SRCS += pwp.c
CODER_SRCS += rgb.c
CODER_SRCS += rla.c
CODER_SRCS += rle.c
CODER_SRCS += sct.c
CODER_SRCS += sfw.c
CODER_SRCS += sgi.c
CODER_SRCS += stegano.c
CODER_SRCS += sun.c
CODER_SRCS += svg.c
CODER_SRCS += tga.c
CODER_SRCS += tiff.c
CODER_SRCS += tile.c
CODER_SRCS += tim.c
CODER_SRCS += topol.c
CODER_SRCS += ttf.c
CODER_SRCS += txt.c
CODER_SRCS += uil.c
CODER_SRCS += url.c
CODER_SRCS += uyvy.c
CODER_SRCS += vicar.c
CODER_SRCS += vid.c
CODER_SRCS += viff.c
CODER_SRCS += wbmp.c
#CODER_SRCS += webp.c
CODER_SRCS += wmf.c
CODER_SRCS += wpg.c
CODER_SRCS += x.c
CODER_SRCS += xbm.c
CODER_SRCS += xc.c
CODER_SRCS += xcf.c
CODER_SRCS += xpm.c
CODER_SRCS += xtrn.c
CODER_SRCS += xwd.c
CODER_SRCS += yuv.c
CODER_SRCS += static.c

HEADERS += $(addprefix $(GMTOP)/coders/, $(CODER_INCS))

SOURCES += $(addprefix $(GMTOP)/coders/, $(CODER_SRCS))



####### jp2 stuff

USR_CFLAGS += -DEXCLUDE_MIF_SUPPORT
USR_CFLAGS += -DEXCLUDE_PNM_SUPPORT
USR_CFLAGS += -DEXCLUDE_BMP_SUPPORT
USR_CFLAGS += -DEXCLUDE_RAS_SUPPORT
USR_CFLAGS += -DEXCLUDE_JPG_SUPPORT
USR_CFLAGS += -DEXCLUDE_PGX_SUPPORT

JP2_BASE_SRCS += jas_cm.c
JP2_BASE_SRCS += jas_debug.c
JP2_BASE_SRCS += jas_getopt.c
JP2_BASE_SRCS += jas_icc.c
JP2_BASE_SRCS += jas_iccdata.c
JP2_BASE_SRCS += jas_image.c
JP2_BASE_SRCS += jas_init.c
JP2_BASE_SRCS += jas_malloc.c
JP2_BASE_SRCS += jas_seq.c
JP2_BASE_SRCS += jas_stream.c
JP2_BASE_SRCS += jas_string.c
JP2_BASE_SRCS += jas_tmr.c
JP2_BASE_SRCS += jas_tvp.c
JP2_BASE_SRCS += jas_version.c

SOURCES += $(addprefix $(GMTOP)/jp2/src/libjasper/base/, $(JP2_BASE_SRCS))

JP2_JP2_SRCS += jp2_cod.c
JP2_JP2_SRCS += jp2_dec.c
JP2_JP2_SRCS += jp2_enc.c

SOURCES += $(addprefix $(GMTOP)/jp2/src/libjasper/jp2/, $(JP2_JP2_SRCS))

JP2_JPC_SRCS += jpc_bs.c
JP2_JPC_SRCS += jpc_cs.c
JP2_JPC_SRCS += jpc_dec.c
JP2_JPC_SRCS += jpc_enc.c
JP2_JPC_SRCS += jpc_math.c
JP2_JPC_SRCS += jpc_mct.c
JP2_JPC_SRCS += jpc_mqcod.c
JP2_JPC_SRCS += jpc_mqdec.c
JP2_JPC_SRCS += jpc_mqenc.c
JP2_JPC_SRCS += jpc_qmfb.c
JP2_JPC_SRCS += jpc_t1cod.c
JP2_JPC_SRCS += jpc_t1dec.c
JP2_JPC_SRCS += jpc_t1enc.c
JP2_JPC_SRCS += jpc_t2cod.c
JP2_JPC_SRCS += jpc_t2dec.c
JP2_JPC_SRCS += jpc_t2enc.c
JP2_JPC_SRCS += jpc_tagtree.c
JP2_JPC_SRCS += jpc_tsfb.c
JP2_JPC_SRCS += jpc_util.c

SOURCES += $(addprefix $(GMTOP)/jp2/src/libjasper/jpc/, $(JP2_JPC_SRCS))


############## jbig

JBIG_HDRS += jbig.h

JBIG_SRCS += jbig.c
JBIG_SRCS += jbig_tab.c

HEADERS += $(addprefix $(GMTOP)/jbig/libjbig/, $(JBIG_HDRS))

SOURCES += $(addprefix $(GMTOP)/jbig/libjbig/, $(JBIG_SRCS))


# LIB_LIBS += bzlib jbig jp2 Magick png ttf webp wmf

# ifeq ($(TIFF_EXTERNAL), NO)
# 	LIB_LIBS += tiff
# else
# 	ifdef TIFF_INCLUDE
# 	USR_INCLUDES += $(addprefix -I, $(TIFF_INCLUDE))
# 	endif
# 	ifdef TIFF_LIB
# 	jpeg_DIR     = $(TIFF_LIB)
# 	LIB_LIBS     += tiff
# 	else
# 	LIB_SYS_LIBS += tiff
# 	endif
# endif

# ifeq ($(JPEG_EXTERNAL), NO)
# 	LIB_LIBS += jpeg
# else
# 	ifdef JPEG_INCLUDE
# 	USR_INCLUDES += $(addprefix -I, $(JPEG_INCLUDE))
# 	endif
# 	ifdef JPEG_LIB
# 	jpeg_DIR     = $(JPEG_LIB)
# 	LIB_LIBS     += jpeg
# 	else
# 	LIB_SYS_LIBS += jpeg
# 	endif
# endif

# ifeq ($(XML2_EXTERNAL), NO)
# 	LIB_LIBS += xml2
# else
# 	ifdef XML2_INCLUDE
# 	USR_INCLUDES += $(addprefix -I, $(XML2_INCLUDE))
# 	endif
# 	ifdef XML2_LIB
# 	xml2_DIR     = $(XML2_LIB)
# 	LIB_LIBS     += xml2
# 	else
# 	LIB_SYS_LIBS += xml2
# 	endif
# endif

# ifeq ($(ZLIB_EXTERNAL),NO)
# 	LIB_LIBS += zlib
# else
# 	ifdef ZLIB_INCLUDE
# 	USR_INCLUDES += $(addprefix -I, $(ZLIB_INCLUDE))
# 	endif
# 	ifdef ZLIB_LIB
# 	z_DIR        = $(ZLIB_LIB)
# 	LIB_LIBS     += z
# 	else
# 	LIB_SYS_LIBS += z
# 	endif
# endif


vlibs:

.PHONY: vlibs
